//
//  Block.cpp
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#include "Block.h"

USING_NS_CC;

Block* Block::createBlock(BlockType type, GridPosition gridPosition)
{
	Block* block = new Block();

	if (block->init())
	{
		block->autorelease();
		block->type = type;
		block->gridPosition = gridPosition;

		return block;
	}

	return nullptr;
}

void Block::setType(BlockType type)
{
	this->type = type;

	if (blockSprite != nullptr && type != BlockType::NONE)
		blockSprite->setSpriteFrame(BlockTypeToFrameName.at(type));
}

void Block::onEnter()
{
	Node::onEnter();

	setContentSize(_BlockSize);
	setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	
	backgroundSprite = Sprite::createWithSpriteFrameName("back.png");
	backgroundSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	backgroundSprite->setPosition(_BlockSize * 0.5f);
	addChild(backgroundSprite, 0);

	blockSprite = Sprite::createWithSpriteFrameName(BlockTypeToFrameName.at(type));
	blockSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	blockSprite->setPosition(_BlockSize * 0.5f);
	addChild(blockSprite, 1);
}

void Block::setActive(bool isActive)
{
	if (isActive)
		backgroundSprite->setSpriteFrame("back_active.png");
	else
		backgroundSprite->setSpriteFrame("back.png");
}

void Block::blink(int times)
{
	backgroundSprite->runAction(Repeat::create(Sequence::create(
											  TintTo::create(0.1f, Color3B::BLACK),
											  TintTo::create(0.1f, Color3B::WHITE)
											  , NULL)
							 , times));
}

void Block::explode()
{
	auto explodeParticles = ParticleSystemQuad::create("assets/explode.plist");
	explodeParticles->setAutoRemoveOnFinish(true);
	explodeParticles->setPosition(getPosition());
	getParent()->addChild(explodeParticles, 10);
}
