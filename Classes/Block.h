//
//  Block.h
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#ifndef Block_hpp
#define Block_hpp

#include <cocos2d.h>
#include "Global.h"

class Block : public cocos2d::Node
{
public:
	static Block* createBlock(BlockType type, GridPosition gridPosition);
	void setActive(bool isActive);
	void blink(int times = 3);
	GridPosition gridPosition;
	BlockType getType()
	{
		return type;
	}
	void setType(BlockType type);
	
	void explode();

	virtual void onEnter();

private:
	BlockType type;
	cocos2d::Sprite* backgroundSprite;
	cocos2d::Sprite* blockSprite;
};

#endif /* Block_hpp */
