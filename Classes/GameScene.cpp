//
//  GameScene.cpp
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#include "GameScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
	auto scene = Scene::create();
	auto gameScene = new GameScene();

	gameScene->autorelease();

	if (gameScene->init())
		scene->addChild(gameScene);
	else
		return nullptr;

	return scene;
}


bool GameScene::init()
{
	if (!Layer::init())
		return false;

	isBisy = false;
	srand(utils::gettime());

	// Preload assets
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("assets/blocks.plist");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("assets/pop.wav");
	Director::getInstance()->getTextureCache()->addImage("assets/bg.jpg");

	// Register event listeners
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(Grid::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(Grid::onTouchMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	auto gridListener = EventListenerCustom::create(EVENT_GRID_READY, CC_CALLBACK_1(GameScene::onGridReady, this));
	_eventDispatcher->addEventListenerWithSceneGraphPriority(gridListener, this);

	auto matchesListener = EventListenerCustom::create(EVENT_GRID_HAS_MATCHES, CC_CALLBACK_1(GameScene::onGridMatches, this));
	_eventDispatcher->addEventListenerWithSceneGraphPriority(matchesListener, this);

	return true;
}

void GameScene::onEnter()
{
	Layer::onEnter();
	
	Size screenSize = Director::getInstance()->getVisibleSize();

	auto background = Sprite::create("assets/bg.jpg");
	background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	background->setPosition(screenSize * 0.5f);
	background->setScaleX(screenSize.width / background->getContentSize().width);
	background->setScaleY(screenSize.height / background->getContentSize().height);
	addChild(background, 0);

	grid = Grid::createGrid(MAX_ROWS, MAX_COLS);
	grid->setPosition(screenSize.width * 0.5f - grid->getContentSize().width * 0.5f,
					  screenSize.height * 0.5f - grid->getContentSize().height * 0.5f - 30);
	addChild(grid, 1);
	
	// HUD
	auto hintButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_pressed.png"),
											 Sprite::createWithSpriteFrameName("button.png"));
	hintButton->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
	hintButton->setPosition(20, screenSize.height - 20);
	auto hintLabel = Label::createWithSystemFont("HINT", "Verdana", 32);
	hintLabel->setPosition(hintButton->getContentSize().width * 0.5f, hintButton->getContentSize().height * 0.5f + 5);
	hintLabel->enableShadow();
	hintButton->addChild(hintLabel);
	hintButton->setCallback(CC_CALLBACK_1(GameScene::showHint, this));


	auto newGameButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("button_pressed.png"),
												Sprite::createWithSpriteFrameName("button.png"));
	newGameButton->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
	newGameButton->setPosition(40 + hintButton->getContentSize().width, screenSize.height - 20);
	auto newGameLabel = Label::createWithSystemFont("NEW GAME", "Verdana", 32);
	newGameLabel->enableShadow();
	newGameLabel->setPosition(newGameButton->getContentSize().width * 0.5f, newGameButton->getContentSize().height * 0.5f + 5);
	newGameButton->addChild(newGameLabel);
	newGameButton->setCallback(CC_CALLBACK_1(GameScene::newGame, this));

	auto menu = Menu::create(hintButton, newGameButton, nullptr);
	menu->setPosition(Vec2::ZERO);
	addChild(menu, 2);

	scoreLabel = Label::createWithSystemFont("", "Verdana", 32);
	scoreLabel->setColor(Color3B::WHITE);
	scoreLabel->enableShadow();
	scoreLabel->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
	scoreLabel->setPosition(screenSize.width - 20, screenSize.height - 20);
	addChild(scoreLabel, 2);

	newGame(this);
}

#pragma mark Touches
bool GameScene::onTouchBegan(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();

	if (isBisy || grid->isBisy() || grid->getBoundingBox().containsPoint(point) == false)
		return false;

	auto block = grid->getBlockForPosition(point);
	if (block == nullptr || block == activeBlock)
		return true;

	if (activeBlock != nullptr)
	{
		// If boxes are neighbours try to swap them
		if (grid->areNeighbours(activeBlock, block))
		{
			swapBoxes(block, activeBlock);
			runAction(Sequence::create(
									   DelayTime::create(0.3f), // Wait for the animation to end
									   CallFunc::create(CC_CALLBACK_0(GameScene::checkForMatches, this, activeBlock, block)),
					  nullptr));

			activeBlock->setActive(false);
			block->setActive(false);
			activeBlock = nullptr;
		}
		else // If not set active the selected one
		{
			activeBlock->setActive(false);
			activeBlock = block;
			activeBlock->setActive(true);
		}
	}
	else
	{
		activeBlock = block;
		activeBlock->setActive(true);
	}

		return true;
}

// Detect swipes
void GameScene::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)
{
	if (isBisy || grid->isBisy() || activeBlock == nullptr)
		return;

	Vec2 touchDistance = touch->getLocation() - touch->getPreviousLocation();
	Block* blockToSwap = nullptr;

	// Right
	if (touchDistance.x >= SWIPE_TRESHOLD && (activeBlock->gridPosition.row + 1 < MAX_ROWS))
		blockToSwap = grid->blocks[activeBlock->gridPosition.row + 1][activeBlock->gridPosition.col];

	// Left
	if (touchDistance.x <= -SWIPE_TRESHOLD && (activeBlock->gridPosition.row > 0))
		blockToSwap = grid->blocks[activeBlock->gridPosition.row - 1][activeBlock->gridPosition.col];

	// Up
	if (touchDistance.y >= SWIPE_TRESHOLD && (activeBlock->gridPosition.col + 1 < MAX_COLS))
		blockToSwap = grid->blocks[activeBlock->gridPosition.row][activeBlock->gridPosition.col + 1];

	// Down
	if (touchDistance.y <= -SWIPE_TRESHOLD && (activeBlock->gridPosition.col > 0))
		blockToSwap = grid->blocks[activeBlock->gridPosition.row][activeBlock->gridPosition.col - 1];

	if (blockToSwap != nullptr)
	{
		swapBoxes(blockToSwap, activeBlock);
		runAction(Sequence::create(
								   DelayTime::create(0.3f), // Wait for the animation to end
								   CallFunc::create(CC_CALLBACK_0(GameScene::checkForMatches, this, activeBlock, blockToSwap)),
								   nullptr));
		
		activeBlock->setActive(false);
		activeBlock = nullptr;
	}

}

void GameScene::checkForMatches(Block* first, Block* second)
{
	bool firstMatches = grid->checkForMatches(first);
	bool secondMatches = grid->checkForMatches(second);

	if (firstMatches == false && secondMatches == false)
	{
		// No matches swap them back and return
		swapBoxes(first, second);
		return;
	}

	if (firstMatches)
	{
		resolveMatchesForBlock(first);
	}

	if (secondMatches)
	{
		resolveMatchesForBlock(second);
	}

	if (firstMatches || secondMatches)
	{
		lockTouches(0.7f);
		grid->fillBlanks();

		activeBlock = nullptr;
	}
}

void GameScene::swapBoxes(Block* first, Block* second)
{
	lockTouches(0.3f);
	grid->swapBoxes(first, second);
	first->runAction(MoveTo::create(0.3f, second->getPosition()));
	second->runAction(MoveTo::create(0.3f, first->getPosition()));
}

void GameScene::resolveMatchesForBlock(Block* block)
{
	if (block == nullptr)
		return;

	auto matches = grid->findMatches(block);
	addScore((int)matches.size());

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("assets/pop.wav");

	for (auto match : matches)
		grid->removeBlockAt(match->gridPosition);

}

void GameScene::newGame(cocos2d::Ref* ref)
{
	grid->removeAllBoxes();
	grid->generateRandomBloxes();
	availableMoves = grid->findAvailableMoves();

	activeBlock = nullptr;
	addScore(-currentScore);

	if (availableMoves.size() == 0)
		newGame(this);

}

// Select a random move from available ones and show it
void GameScene::showHint(cocos2d::Ref *ref)
{	
	if (!grid->isBisy() && availableMoves.size() > 0)
	{
		GridMove move = availableMoves.at(random(0, (int)availableMoves.size() - 1));
		grid->blocks[move.first.row][move.first.col]->blink();
		grid->blocks[move.second.row][move.second.col]->blink();
	}
}

void GameScene::lockTouches(float time)
{
	unschedule(CC_SCHEDULE_SELECTOR(GameScene::unlockTouches));
	scheduleOnce(CC_SCHEDULE_SELECTOR(GameScene::unlockTouches), 0.5f);
	isBisy = true;
}

void GameScene::unlockTouches(float dt)
{
	isBisy = false;
}

void GameScene::addScore(int score)
{
	currentScore += score;
	char score_string[512];
	snprintf(score_string, 512, "Score: %d", currentScore);
	scoreLabel->setString(score_string);
}


void GameScene::onGridReady(cocos2d::EventCustom *event)
{
	availableMoves = grid->findAvailableMoves();
	if (availableMoves.size() == 0)
		newGame(this);
}

void GameScene::onGridMatches(cocos2d::EventCustom* event)
{
	EventMatchesData *em = (EventMatchesData *) event->getUserData();
	addScore(em->matches);
}
