//
//  GameScene.h
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#ifndef GameScene_hpp
#define GameScene_hpp

#include <cocos2d.h>
#include "Grid.h"

class GameScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	virtual void onEnter();

private:
	Block* activeBlock;
	Grid* grid;
	bool isBisy;
	cocos2d::Label* scoreLabel;
	std::vector<GridMove> availableMoves;
	int currentScore;

	void swapBoxes(Block* first, Block* second);
	void newGame(cocos2d::Ref* ref);
	void showHint(cocos2d::Ref* ref);
	void checkForMatches(Block* first, Block* second);
	void lockTouches(float time);
	void unlockTouches(float dt);
	void addScore(int score);
	void resolveMatchesForBlock(Block* block);

	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	void onGridReady(cocos2d::EventCustom* event);
	void onGridMatches(cocos2d::EventCustom* event);
};

#endif /* GameScene_hpp */
