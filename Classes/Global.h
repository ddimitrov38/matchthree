//
//  Global.h
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#ifndef constants_h
#define constants_h

#include <cocos2d.h>

#define EVENT_GRID_READY "event.custom.grid_is_ready"
#define EVENT_GRID_HAS_MATCHES "event.custom.grid_has_matches"

const int MAX_MATCHES = 3; // Number of matches to consider a succesfull match

// Size of the grid
const int MAX_ROWS = 7;
const int MAX_COLS = 7;

const int SWIPE_TRESHOLD = 5; // Minimum pixels for swipe move

const cocos2d::Size _BlockSize = cocos2d::Size(80, 80);

enum class BlockType : int
{
	NONE = -1,
	RED = 0,
	BLUE,
	GREEN,
	YELLOW,
	PURPLE
};

const std::map<BlockType, std::string> BlockTypeToFrameName
{
	{BlockType::RED, "red.png"},
	{BlockType::BLUE, "blue.png"},
	{BlockType::GREEN, "green.png"},
	{BlockType::YELLOW, "yellow.png"},
	{BlockType::PURPLE, "purple.png"}
};
	
typedef struct {
	int row;
	int col;
} GridPosition;
	
typedef struct {
	GridPosition first;
	GridPosition second;
} GridMove;
	
typedef struct {
	int matches;
} EventMatchesData;

#endif /* constants_h */
