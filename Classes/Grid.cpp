//
//  Grid.cpp
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#include "Grid.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Grid* Grid::createGrid(int rows, int cols)
{
	Grid* grid = new Grid();

	if (grid->init())
	{
		grid->autorelease();

		grid->setContentSize(Size(rows * _BlockSize.width, cols * _BlockSize.height));
		grid->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

		return grid;
	}

	return nullptr;
};

void Grid::onEnter()
{
	Layer::onEnter();
}

void Grid::generateRandomBloxes()
{
	for (int col = 0; col < MAX_COLS; col++)
	{
		for (int row = 0; row < MAX_ROWS; row++)
		{
			Block* block = Block::createBlock((BlockType)random((int)BlockType::RED, (int)BlockType::PURPLE), {row, col});

			// Make sure there are no initial groups of matches
			while(checkForMatches(block) == true)
				block->setType((BlockType)random((int)BlockType::RED, (int)BlockType::PURPLE));

			block->setPosition(row * _BlockSize.width + _BlockSize.width * 0.5f,
							   col * _BlockSize.height + _BlockSize.height * 0.5f);

			addChild(block, 1);
			blocks[row][col] = block;
		}
	}
}

void Grid::removeAllBoxes()
{
	stopAllActions();
	bisy = false;

	for (int col = 0; col < MAX_COLS; col++)
		for (int row = 0; row < MAX_ROWS;row++)
		{
			if (blocks[row][col] != nullptr)
			{
				removeChild(blocks[row][col]);
				blocks[row][col] = nullptr;
			}
		}
}

Block* Grid::getBlockForPosition(cocos2d::Vec2 position)
{
	unsigned int row = (unsigned int) ceil((position.x - getPosition().x) / _BlockSize.width) - 1;
	unsigned int col = (unsigned int) ceil((position.y - getPosition().y) / _BlockSize.height) - 1;

	CCASSERT((col < MAX_COLS && row < MAX_ROWS), "Incorrect row/col. Index out of range");

	if (blocks[row][col])
		return blocks[row][col];

	return nullptr;
}

void Grid::swapBoxes(Block* first, Block* second)
{
	if (!first || !second)
		return;
	
	if (!areNeighbours(first, second))
	{
		CCLOGERROR("Blocks [%d, %d] and [%d, %d] are not neighbours!",
				   first->gridPosition.row,
				   first->gridPosition.col,
				   second->gridPosition.row,
				   second->gridPosition.col
				   );
		return;
	}

	blocks[first->gridPosition.row][first->gridPosition.col] = second;
	blocks[second->gridPosition.row][second->gridPosition.col] = first;

	// Swap bloxes indexes
	GridPosition temp = first->gridPosition;
	first->gridPosition = second->gridPosition;
	second->gridPosition = temp;	
}

void Grid::removeBlockAt(GridPosition pos)
{
	Block* block = blocks[pos.row][pos.col];
	if (block)
	{
		blocks[pos.row][pos.col] = nullptr;
		block->explode();
		block->setType(BlockType::NONE);
		removeChild(block, true);
	}
}

bool Grid::areNeighbours(Block* first, Block* second)
{
	if (!first || !second)
		return false;

	int x = abs(first->gridPosition.row - second->gridPosition.row);
	int y = abs(first->gridPosition.col - second->gridPosition.col);

	return (x == 1 && y == 0) || (x == 0 && y == 1);
}

bool Grid::checkForMatches(Block* block)
{
	if (!block)
		return false;

	GridPosition gp = block->gridPosition;

	// Check horizontal
	int matches = 1;
	int row = gp.row;
	while(--row > -1 && blocks[row][gp.col] && blocks[row][gp.col]->getType() == block->getType()) // Check left
		matches++;

	row = gp.row;
	while(++row < MAX_ROWS && blocks[row][gp.col] && blocks[row][gp.col]->getType() == block->getType()) // Check right
		matches++;

	if (matches >= MAX_MATCHES)
		return true;

	// Check vertical
	matches = 1;
	int col = gp.col;
	while(--col > -1 && blocks[gp.row][col] && blocks[gp.row][col]->getType() == block->getType()) // Check down
		matches++;

	col = gp.col;
	while(++col < MAX_COLS && blocks[gp.row][col] && blocks[gp.row][col]->getType() == block->getType()) // Check up
		matches++;

	return (matches >= MAX_MATCHES);
}


std::vector<Block*> Grid::findMatches(Block* block, bool isRoot)
{
	if (isRoot)
		matches.clear();
	
	matches.push_back(block);

	GridPosition pos = block->gridPosition;

	Block* leftBlock = (pos.row > 0)?blocks[pos.row - 1][pos.col]:nullptr;
	Block* rightBlock = (pos.row < MAX_ROWS - 1)?blocks[pos.row + 1][pos.col]:nullptr;
	Block* topBlock = (pos.col < MAX_COLS - 1)?blocks[pos.row][pos.col + 1]:nullptr;
	Block* bottomBlock = (pos.col > 0)?blocks[pos.row][pos.col - 1]:nullptr;

	// Left
	if (leftBlock && leftBlock->getType() == block->getType())
	{
		if (std::find(matches.begin(), matches.end(), leftBlock) == matches.end())
			findMatches(leftBlock, false);
	}

	// Right
	if (rightBlock && rightBlock->getType() == block->getType())
	{
		if (std::find(matches.begin(), matches.end(), rightBlock) == matches.end())
			findMatches(rightBlock, false);
	}

	// Top
	if (topBlock && topBlock->getType() == block->getType())
	{
		if (std::find(matches.begin(), matches.end(), topBlock) == matches.end())
			findMatches(topBlock, false);
	}

	// Bottom
	if (bottomBlock && bottomBlock->getType() == block->getType())
	{
		if (std::find(matches.begin(), matches.end(), bottomBlock) == matches.end())
			findMatches(bottomBlock, false);
	}

	return matches;
}


std::vector<GridMove> Grid::findAvailableMoves() {
	std::vector<GridMove> availableMoves;
	Block* otherBlock;

	for (int row = 0; row < MAX_ROWS; row++)
	{
		for (int col = 0; col < MAX_COLS; col++)
		{
			Block *block = blocks[row][col];
			if (block == nullptr)
				continue;

			// Swap right and check if successfull, than swap them back
			if (row < MAX_ROWS - 1 && blocks[row + 1][col])
			{
				otherBlock = blocks[row + 1][col];
				swapBoxes(block, otherBlock);
				if (checkForMatches(block) || checkForMatches(otherBlock))
					availableMoves.push_back({otherBlock->gridPosition, block->gridPosition});
				swapBoxes(otherBlock, block);
			}

			// Swap up and check
			if (col < MAX_COLS - 1 && blocks[row][col + 1])
			{
				otherBlock = blocks[row][col + 1];
				swapBoxes(block, otherBlock);
				if (checkForMatches(block) || checkForMatches(otherBlock))
					availableMoves.push_back({otherBlock->gridPosition, block->gridPosition});
				swapBoxes(otherBlock, block);
			}
		}
	}

	return availableMoves;
}


void Grid::fillBlanks()
{
	bisy = true;
	std::vector<Block*> addedBlocks;
	for (int row = 0; row < MAX_ROWS; row++)
	{
		int blanks = 0;

		// Find blank spots for the column
		for (int col = 0; col < MAX_COLS; col++)
		{
			if (blocks[row][col] == nullptr)
				blanks++;
			else if (blanks > 0) // Move blocks above the blank down
			{
				int new_col = col - blanks;
				blocks[row][col]->runAction(EaseBounceOut::create(
																  MoveTo::create(0.7f,
																				 Vec2(blocks[row][col]->getPositionX(),
																					  new_col * _BlockSize.height + _BlockSize.height * 0.5f)
																				 )));
				blocks[row][new_col] = blocks[row][col];
				blocks[row][col] = nullptr;
				blocks[row][new_col]->gridPosition = {row, new_col};
				
				addedBlocks.push_back(blocks[row][new_col]);
			}
		}

		// Add new blocks to fill the missing ones
		while(blanks--) {
			int new_col = MAX_COLS - blanks - 1;
			Block* block = Block::createBlock((BlockType)random((int)BlockType::RED, (int)BlockType::PURPLE), {row, new_col});

			Vec2 newPosition = Vec2(row * _BlockSize.width + _BlockSize.width * 0.5f,
									new_col * _BlockSize.height + _BlockSize.height * 0.5f);

			block->setPositionX(newPosition.x);
			block->setPositionY(getContentSize().height + (MAX_COLS - blanks) * _BlockSize.height * 0.5f);
			block->gridPosition = {row, new_col};
			block->runAction(EaseBounceOut::create(MoveTo::create(0.7f, newPosition)));

			addChild(block, 1);
			blocks[row][new_col] = block;
			addedBlocks.push_back(block);
		}

	}
	
	runAction(Sequence::create(
							   DelayTime::create(0.75f),
							   CallFunc::create(CC_CALLBACK_0(Grid::resolveMatchesForBlocks, this, addedBlocks)),
							   NULL));
}

void Grid::resolveMatchesForBlocks(std::vector<Block*> blocks)
{
	bool hasMatches = false;
	std::vector<Block*> blocksToRemove;

	for (int i = 0; i < blocks.size(); i++)
	{
		Block* block = blocks.at(i);

		if (block && block->getType() != BlockType::NONE && checkForMatches(block))
		{
			hasMatches = true;
			for (auto match : findMatches(block))
			{
				if (std::find(blocksToRemove.begin(), blocksToRemove.end(), match) == blocksToRemove.end())
					blocksToRemove.push_back(match);
			}
		}
	}
	

	if (hasMatches) {
		EventCustom hasMatchesEvent(EVENT_GRID_HAS_MATCHES);
		EventMatchesData em;
		em.matches = (int) blocksToRemove.size();
		hasMatchesEvent.setUserData((void *) &em);
		_eventDispatcher->dispatchEvent(&hasMatchesEvent);

		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("assets/pop.wav");
		for (auto block : blocksToRemove)
			removeBlockAt(block->gridPosition);

		fillBlanks();
	}
	else
	{
		EventCustom gridReadyEvent(EVENT_GRID_READY);
		_eventDispatcher->dispatchEvent(&gridReadyEvent);

		bisy = false;
	}
}
