//
//  Grid.h
//  MatchThree
//
//  Created by Dimitar Dimitrov on 5/2/16.
//
//

#ifndef Grid_hpp
#define Grid_hpp

#include <cocos2d.h>
#include "Global.h"
#include "Block.h"

class Grid: public cocos2d::Layer
{
public:
	Block* blocks[MAX_ROWS][MAX_COLS];

	static Grid* createGrid(int rows, int cols);

	void removeAllBoxes();
	void generateRandomBloxes();
	Block* getBlockForPosition(cocos2d::Vec2 position);
	bool areNeighbours(Block* first, Block*second);
	void swapBoxes(Block* first, Block* second);
	bool checkForMatches(Block* block);
	std::vector<Block*> findMatches(Block* block, bool isRoot = true);
	void removeBlockAt(GridPosition pos);
	void fillBlanks();
	void resolveMatchesForBlocks(std::vector<Block*> blocks);

	std::vector<GridMove> findAvailableMoves();

	virtual void onEnter();
	bool isBisy() { return bisy; };

private:
	std::vector<Block*> matches;
	bool bisy;
};

#endif /* Grid_hpp */
